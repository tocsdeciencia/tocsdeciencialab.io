---
layout: default
title: Tallers creatius
---
<div class="w3-container">
<h1>
Tallers creatius
</h1>
</div>

<div class="w3-container">
<p>Tallers creatius adreçats a alumnes d'educació primària i 
secundària</p>
</div>



<div class="w3-row">
  
  <div class="w3-threequarter">


<section class="w3-container w3-section">
  <article id="taller-leds" class="w3-card-4 w3-hover-opacity ">
    <header class="w3-container w3-light-grey">
      <h3>Una postal de Nadal amb LEDs</h3>
    </header>
    <div class="w3-container">
      <img src="/assets/img/Postal_LED.jpg" class="w3-circle w3-left w3-padding" alt="Gominoles" style="max-width:30%">
     <p>Per què s'encén el llum quan premem l'interruptor? En aquest taller aprendrem els elements bàsics d'un circuit elèctric i el seu funcionament. Construirem el nostre propi circuit elèctric que ens servirà per il·luminar una felicitació de Nadal.</p>

      <p>Durada: 1 hora i 30 minuts</p>
      <p>Nivells recomanats: cicle superior de Primària i primer cicle d'ESO</p>
      </div>
</article>
</section>

    
<section class="w3-container w3-section">
  <article class="w3-card-4 w3-hover-opacity ">
    <header class="w3-container w3-light-grey">
      <h3>Additius alimentaris</h3>
    </header>
    <div class="w3-container">
      <img src="/assets/img/additius.png" class="w3-circle w3-left w3-padding" alt="Gominoles" style="max-width:30%">
     <p>Aprendrem a desxifrar les etiquetes dels productes
alimentaris. Veurem els difer ents tipus d'additius i demostrarem les
seves funcions. També es reflexionarà so bre el tipus d'additiu més
comú a la indústria alimentària: els edulcorants. Ref lexionarem sobre
els additius que incorporen els aliments preparats. Finalment, cada
alumne elaborarà un colorant natural i aprendrà a utilitzar-lo.
     </p>
     </div>
  </article>
</section>


</div>

</div>


<div class="w3-container">
<h2 id="escoles">Tallers per a escoles</h2>

</div>


<div class="w3-container">
<h2 id="lesampa">Tallers per a les AMPA</h2>

</div>
