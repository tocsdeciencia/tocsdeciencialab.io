---
layout: default
title: El projecte
---
<div class="w3-container">
<h1>
El projecte
</h1>
</div>


  
<section class="w3-container w3-section" style="max-width: 600px; margin-left: auto; margin-right: auto;"> 
<h1>Què és Tocs de ciència?</h1>
<p>Tocs de ciència és un projecte de divulgació científica que vol fer descobrir als alumnes de primària com la ciència, la tecnologia i les matemàtiques són per tot arreu i ens ajuden a conèixer i comprendre el món que vivim.
</p>
<p>Les activitats que s'ofereixen en el marc d'aquest projecte també volen contribuir a:
<ul>
 <li>estimular la creativitat artística, la reflexió i el pensament crític</li>
 <li>desenvolupar les competències STEAM (Ciència, Tecnologia, Enginyeria, Art i Matemàtiques)</li>
 <li>impulsar  el canvi educatiu cap al marc d'escola avançada</li>
</ul>
</p>
</section>


{% include nuriatorras.html %}
