---
layout: default
title: Inici
---
<div class="w3-display-container">
    <!-- <div class="w3-btn-floating w3-deep-orange w3-hover-dark-grey w3-display-left" onclick="plusDivs(-1)">&#10094;</div> -->
    <!-- <div class="w3-btn-floating w3-deep-orange w3-hover-dark-grey w3-display-right" onclick="plusDivs(1)">&#10095;</div> -->

  <div class="mySlides"><a href=""><img src="/assets/img/fons2.png" alt="Logo Tocs de Ciència"><!-- <div class="w3-display-bottomright w3-container w3-padding-16 w3-deep-orange w3-hide-small">Tocs de Ciència</div> --></a></div>
  <div class="mySlides"><a href="/serveis.html"><img src="/assets/img/art-supplies.jpg" alt="Material tallers"><!-- <div class="w3-display-bottomright w3-container w3-padding-16 w3-deep-orange w3-hide-small">Tallers</div> --></a></div>
  <div class="mySlides"><a href="/contacte.html"><img src="/assets/img/peu.png" alt="Mans Tocs de Ciència"><!-- <div class="w3-display-bottomright w3-container w3-padding-16 w3-deep-orange w3-hide-small">Tocs de Ciència</div> --></a></div>
  <div class="w3-center w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%">
    <span class="w3-badge mySlidesRound w3-border w3-transparent w3-hover-deep-orange" onclick="currentDiv(1)"></span>
    <span class="w3-badge mySlidesRound w3-border w3-transparent w3-hover-deep-orange" onclick="currentDiv(2)"></span>
    <span class="w3-badge mySlidesRound w3-border w3-transparent w3-hover-deep-orange" onclick="currentDiv(3)"></span>
  </div>
</div>
<script src="/assets/js/script-mySlides.js" charset="utf-8"></script>

<div class="w3-row">
  
<div class="w3-threequarter">


<section class="w3-container w3-section" style="max-width: 600px; margin-left: auto; margin-right: auto;"> 
<h1>Què és Tocs de ciència?</h1>
<p>Tocs de ciència és un projecte de divulgació científica que vol fer descobrir als alumnes de primària com la ciència, la tecnologia i les matemàtiques són per tot arreu i ens ajuden a conèixer i comprendre el món que vivim.
</p>
<p>Les activitats que s'ofereixen en el marc d'aquest projecte també volen contribuir a:
<ul>
 <li>estimular la creativitat artística, la reflexió i el pensament crític</li>
 <li>desenvolupar les competències STEAM (Ciència, Tecnologia, Enginyeria, Art i Matemàtiques)</li>
 <li>impulsar  el canvi educatiu cap al marc d'escola avançada</li>
</ul>
</p>
</section>

  
<!-- <section class="w3-container w3-section"> -->
<!--   <h2><i class="fa fa-cog w3-spin w3-green" ></i> Nous tallers!</h2> -->

<!--   <div class="w3-card-4 w3-hover-opacity "> -->
<!--     <a href="tallers#taller-leds" style="text-decoration: none;"> -->
<!--     <header class="w3-container w3-light-grey"> -->
<!--       <h3>Una postal de Nadal amb LEDs</h3> -->
<!--     </header> -->
<!--     <div class="w3-container"> -->
<!--       <img src="Postal_LED.jpg" class="w3-circle w3-left w3-padding" alt="Gominoles" style="max-width:30%"> -->
<!--      <p>Per què s'encén el llum quan premem l'interruptor? En aquest taller aprendrem els elements bàsics d'un circuit elèctric i el seu funcionament. Construirem el nostre propi circuit elèctric que ens servirà per il·luminar una felicitació de Nadal. -->
<!--      </p> -->
<!--     </div> -->
<!--    </a> -->
<!--   </div> -->
<!--   <div class="w3-container w3-section"> -->
<!--     <a href="tallers">Vegeu tots els tallers</a> -->
<!--   </div> -->
<!-- </section> -->


<section class="w3-container w3-section">
  <h2>Darrera entrada al blog</h2>
<ul>
  {% assign first_posts = site.posts | slice:0, 1 %}
  {% for post in first_posts %}
    <li>
      <h2><a href="{{ post.url }}">{{ post.title }}</a></h2>
      <p>{{ post.excerpt }}</p>
    </li>
  {% endfor %}
</ul>
<div class="w3-container w3-section">
    <a href="/blog.html">Vegeu totes les entrades</a>
</div>
</section>


</div>

<aside class="w3-container w3-quarter w3-border-left w3-section">
 <a class="twitter-timeline" data-tweet-limit="5" data-theme="light" data-link-color="#19CF86" href="https://twitter.com/tocsdeciencia">Tweets by tocsdeciencia</a>
</aside>

</div>
