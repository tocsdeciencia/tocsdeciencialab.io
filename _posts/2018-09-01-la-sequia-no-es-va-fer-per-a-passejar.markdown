---
layout: post
title:  "La sèquia no es va fer per a passejar"
date:   2018-09-01 10:00:00 +0100
author: Núria Torras Melenchón
tags: [opinió, Manresa, la Sèquia]
---
El paisatge urbà de Manresa no seria així si no fos per la Sèquia. La ciutat va 
créixer aprofitant l'existència d'aquest canal i els principals carrers i l'actu
al xarxa de clavegueram ressegueixen el traçat d'antics recs de la Sèquia. Un fe
t únic que fa de Manresa un indret singular i amb personalitat pròpia.

Ara unes finques de regadiu i un tram històric de la Sèquia estan afectats direc
tament pel pla parcial urbanístic Sagrada Família. Un pla que va néixer per aten
dre unes previsions de creixement demogràfic de la població manresana que el tem
ps ha demostrat que eren desorbitades. El pla urbanístic, però, segueix endavant
 i dotarà la ciutat de nous habitatges per damunt de les necessitats reals de la
 població.

D'acord amb aquest pla urbanístic, part del ramal de la Sèquia de Santa Clara es convertirà en un espai públic en forma de parc i els canals de reg i les seves derivacions desapareixeran. Després ens proposaran d'anar al parc o que anem a buscar el canal als afores de la ciutat i que ens hi passegem en el nostre temps lliure. I, de fet, som uns afortunats de poder tenir una xarxa de camins verds prop de casa per gaudir de la natura i del paisatge. Però aleshores estem atorgant uns nous valors i no parlem de la mateixa Sèquia. La Sèquia no es va construir al segle XIV per anar-hi a passejar. De fet, allò que la fa tan valuosa avui dia és justament que encara manté la seva funció original de regadiu set segles després de la seva construcció. Set segles. M'atreviria a dir que es podran comptar amb una sola mà les obres d'enginyeria d'avui dia d'aquesta envergadura que estaran encara en funcionament l'any 2700.

Alhora el terreny que s'urbanitzarà no és pas un solar buit sense cap mena d'ús. Es tracta majoritàriament de terres de conreu regades per un ramal principal de la Sèquia i les seves derivacions de petits canals. Una activitat realitzada en un entorn natural que millora el benestar i la qualitat de vida de molta gent gran del barri i que afavoreix la unió entre veïns. Manresa perdrà bona part del seu regadiu tradicional i històric, mentre que a altres pobles i ciutats com ara Santpedor, sense anar més lluny, engeguen o ja tenen en marxa projectes municipals d'horts urbans per fomentar la tradició hortícola, l'hort d'autoconsum i el consum de productes de proximitat o quilòmetre 0.

El pla parcial es va aprovar ara fa deu anys però és l'actual Ajuntament qui té a les seves mans decidir què fer amb el regadiu de Manresa i amb el patrimoni de la Sèquia.
