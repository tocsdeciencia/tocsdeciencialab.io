---
layout: post
title:  "La química és clara si s'explica bé"
date:   2013-11-09 08:23:00 +0100
author: Núria Torras Melenchón
tags: projecte Cardener-Llobregat pesticides
---
La manresana rep un guardó de Basf per trobar parts de les restes de pesticides a la conca Cardener-Llobregat.

Entrevista apareguda a <https://www.regio7.cat/manresa/2013/11/07/quimica-clara-sexplica/249140.html>

