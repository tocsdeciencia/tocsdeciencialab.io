---
layout: default
title: Blog
---
<section class="w3-container w3-section" style="max-width: 600px; margin-left: auto; margin-right: auto;">
<h1>Entrades al blog</h1>

<ul>
  {% for post in site.posts %}
    <li>
      <h2><a href="{{ post.url }}">{{ post.title }}</a></h2>
      <p>{{ post.excerpt }}</p>
    </li>
  {% endfor %}
</ul>
</section>
