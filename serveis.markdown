---
layout: default
title: Què us oferim
estil: ".w3-card-4 {
  min-height: 292px;
  max-width: 500px;
}
.w3-card-4 img {width: 200px;}
"
---
<div class="w3-container">
<h1>
Què us oferim?
</h1>
</div>


<div class="w3-container w3-section w3-center w3-row-padding">

<article class=" w3-half">
<div class="w3-card-4 w3-content w3-section">
<header class="w3-container w3-light-grey w3-center">
  <h3>Material divulgatiu</h3>
</header>

<div class="w3-container">
  <img src="/assets/img/Recursos2.png" alt="Avatar" class="w3-left w3-circle w3-margin-right">
  <hr>
  <p>Disseny i confecció de materials divulgatius en diversos formats per treballar a l'aula</p>
  <br>
</div>
</div>
</article>
  
<article class=" w3-half">
<div class="w3-card-4 w3-content w3-section">
<header class="w3-container w3-light-grey w3-center">
  <h3>Activitats per a les escoles</h3>
</header>

<div class="w3-container">
  <img src="/assets/img/Tallers.png" alt="Avatar" class="w3-left w3-circle w3-margin-right">  
  <hr>
  <p>Planificació i implementació de programes educatius basats en el treball per projectes</p>
</div>
<a href="/cataleg_tallers.pdf" class="w3-btn-block w3-dark-grey w3-padding-8">+ Descarregueu el catàleg</a>
</div>
</article>

<article class="w3-half">
<div class="w3-card-4 w3-content w3-section">
<header class="w3-container w3-light-grey w3-center">
  <h3>Activitats per a les AMPA</h3>
</header>

<div class="w3-container">
  <img src="/assets/img/Materials.png" alt="Avatar" class="w3-left w3-circle w3-margin-right">  
  <hr>
  <p>Disseny i dinamització d'itineraris i activitats creatives per a infants i joves</p>
  <br>
</div>
<a href="/cataleg_ampa.pdf" class="w3-btn-block w3-dark-grey w3-padding-8">+ Descarregueu el catàleg</a>
</div>
</article>


</div>
